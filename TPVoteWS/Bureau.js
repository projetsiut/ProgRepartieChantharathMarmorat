// Exécute la partie serveur d'un bureau pour un électeur dont le nom et le mot de passe sont donnés
// sur la ligne de commande
// Reçoit les votes de tous les bureaux mais ne renvoie les résultats finaux qu'au bureau local

const process = require('process'); // pour argv et exit
const WebSocket = require('ws');    // WebSocket
const crypto = require('crypto');   // cryptage des mots de passe

// ------- Initialisation de la question et de la liste électorale
const config = require('./Config.js.local4');
var laListe = config.listeElectorale();
var laQuestion = config.question();

// ------- Tableau des résultats, un par réponse, tous initialisés à 0
var nbVotants = 0;
var resultats = [];
var aVote = false;

for (var i = 0; i < laQuestion.nbChoix; i++) {
  resultats.push(0);
}

// -------- Analyse des arguments (nom, mdp, portHTTP), vérification de l'authentification et création du bureau
if (process.argv.length != 5) {
    console.error("Usage : node Bureau.js Nom Mdp portHTTP");
    process.exit(1);
}
var nom = process.argv[2];
var mdp = crypto.createHash('md5').update(process.argv[3]).digest("hex").toUpperCase();

var monBureau = laListe.findIndex(function(elm){
  return elm.nom === nom;
});

if (monBureau == -1) {
    console.error("Authentification incorrecte : " + nom);
    process.exit(1);
}

// Le port de service Web, a priori 8000
var portHTTP = parseInt(process.argv[4]);
console.log("Bureau ouvert pour", nom);

// ------- Configuration du serveur Web pour l'interface utilisateur dans le navigateur
const http = require('http');                // pour traiter les requêtes et envoyer les réponses
const swig  = require('derer');              // langage de template (genre de Twig)
const url  = require('url');                 // analyse des URL des requêtes
const querystring  = require('querystring'); // paramètres GET
const fs  = require('fs');                   // FileSystem, pour lire les fichiers JS et CSS

// Renvoie un fichier CSS ou JS vers le navigateur
// type = "text/css" ou "application/javascript");
function sendFile(res, path, type) {
    fs.readFile(__dirname + path, 'utf8', function (error, data) {
        if (error) {
            console.log(error.stack, "fichier : ", path);
            res.writeHead(404, {'Content-Type': "text/html; charset=utf-8"});
        } else {
            res.writeHead(200, {'Content-Type': type + "; charset=utf-8"});
            res.write(data);
        }
        res.end();
    });
}

// Définition du comportement du serveur selon l'URL
function service(req, res) {
    var monURL = url.parse(req.url);
    // console.log(monURL);
    var page = decodeURI(monURL.pathname);
    console.log(page); // par exemple "/" ou "/vote.css", "/vote.js"
    var output;

    if (page === '/') {
      res.writeHead(200, {'Content-Type': "text/html; charset=utf-8"});
      res.write(bureauUI({'titre':'Bureau de vote','port':laListe[monBureau].local,'nom':nom}));
      res.end();
    } else if (page === '/vote.css') {
      sendFile(res, page, 'text/css')
    }else if (page === '/vote.js') {
      sendFile(res, page, 'application/javascript')
    }
}

var wssClient;

function handleConnexion(sock) {
    wssClient = sock;
  sock.send(JSON.stringify(laQuestion));
  sock.send(JSON.stringify(resultats));

  sock.on('message', function (message) {
      if(!aVote){
          resultats[message]+=1;
          sock.send(JSON.stringify(resultats));
          for (let bureau of laListe){
              if(bureau !== monBureau){
                  let ws = new WebSocket('ws://localhost:'+bureau.port+"/Bureau.js");
                  ws.onerror = function (event) {
                      console.log("Port non ouvert ",bureau.port)
                  };
                  ws.onopen = function (event) {
                      ws.send(JSON.stringify(resultats))
                  }

              }
          }
          aVote = true;
      }

  });
}

function handleConnexionB(sock) {    
    sock.on('message',function (message) {
        resultats = JSON.parse(message);
        let sum = resultats.reduce((a,b)=>{return a+b},0);

        if(sum === laListe.length)
            wssClient.send(JSON.stringify({fini:true}));

    })
}

// Compilation du template dans une fonction JavaScript
const bureauUI = swig.compileFile('templates/Bureau.html');
// Création de l'objet serveur Web (comportement déplacé dans la fonction service)
const serv = http.createServer(service);
// Lancement de l'écoute sur le port portHTTP
serv.listen(portHTTP);

const wss = new WebSocket.Server({port : laListe[monBureau].local });
const wssB = new WebSocket.Server({port : laListe[monBureau].port});
wss.on('connection', handleConnexion);
wssB.on('connection', handleConnexionB);
