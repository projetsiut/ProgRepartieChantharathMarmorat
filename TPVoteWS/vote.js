var laQuestion; // Téléchargée depuis le bureau

var ws; // Socket pour la connexion locale

function pose(question) {
    var fieldset = $('fieldset');
    fieldset.children().eq(0).text(question.libellé);
    question.reponses.forEach(function (r, indice) {
        var input = $('<input type="radio" name="reponse">')
                     .attr('value', indice)
                     .attr('id', "i"+indice);
        var label = $('<label>').attr('for', "i"+indice).append(r);
        fieldset.append(input).append(label).append('<br/>');
    });
}

function afficheResultats(res) {
    var aCloner = $('#resultats > thead > tr:last-child');
    var tbody = $('#resultats > tbody');
    tbody.empty(); // pas forcément obligatoire si on n'affiche les résultats qu'une fois à la fin
    res.forEach(function (valeur, indice) {
        var tr = aCloner.clone(true).removeAttr('style');
        tr.children().eq(0).text(laQuestion.reponses[indice]);
        tr.children().eq(1).text(valeur);
        tbody.append(tr);
    });
}

function voter () {
    // on vérifie qu'une case est cochée
    var radios = $('input[name=reponse]');
    var numReponse = -1;
    radios.each(function (indice, current) {
        if (current.checked) {
            numReponse = indice;
        }
    });
    if (numReponse == -1) {
        alert("Il faut sélectionner une réponse");
    } else {
        // Transmettre le vote au serveur local
        ws.send(numReponse);
        // Masquer le bouton de vote
        $('#valider').hide();
    }
}

$(document).ready(function (){
    var question = false;

    ws = new WebSocket("ws://localhost:"+PORTLOCAL+"/Bureau.js");
    ws.onmessage = function(message) {
        let data = JSON.parse(message.data);
        console.log(message);
        if (data.fini !== undefined){
            alert('Le vote est fini');
        } else {
            if (!question) {
                laQuestion = data;
                pose(laQuestion);
                question = true;
            } else {
                afficheResultats(data);
            }
        }
    };


    $('#valider').click(voter);
});
