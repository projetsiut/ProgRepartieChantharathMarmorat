#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>          /* Pour pid_t (fork, getpid, getppid) */
#include <sys/socket.h>
#include "Reseau.h"

#define MAX 512

int pam[2], map[2];                 /* les deux descripteurs de fichier associés au tube */

//Correspond au "Thread main" du sujet, lit sur le socket et envoie vers morpho
void* readerThread(void* arg){
  //Récupère le socket depuis le processurs père
  int msgSock = *((int*)arg);
  int lus;
  char* texte = (char*)calloc(MAX,sizeof(char));

  while (1) {
      memset(texte, 0, MAX);
      lus = read(msgSock,texte,MAX);
      printf("Lus %d\n", lus);
      fflush(stdout);
      //Test la lecture sur le socket
      if (lus == -1) {
        perror("Read from socket : ");
        break;
      }

      //Break si texte vide
      if (lus == 0) { // On met == 1 pour \n
        write(msgSock,"Deconnexion\n",12);
        break;
      }

      //Test l'écriture vers morpho
      if (write(pam[1],texte,lus) == -1) {
        perror("Write to morpho : ");
        break;
      }
  }
  free(texte);
  printf("Fin thread reader\n");
  return NULL;
}

//Correspond au "Thread fils" du sujet, lis depuis morpho et envoie vers le client
void* writerThread(void* arg){
  int msgSock = *((int*)arg);
  char result[MAX];
  int lus;

  while (1) {
    memset(result, 0, MAX);
    //Lis depuis le tube correspondant à la sortie de morpho
    lus = read(map[0],result,MAX);
    printf("Lus %d\n", lus);
    fflush(stdout);
    if (lus == -1) {
      perror("Write to socket : ");
      break;
    }

    if (write(msgSock,result,lus) == -1) {
      perror("Write to socket : ");
      break;
    }
  }
  printf("Fin thread writer\n");
  return NULL;
}

//Plus ou moins identique à l'exercice précédent
int main(int argc, char const *argv[]) {
  int port = 2553;
  int socketNum,msgSock;
  pthread_t wthread, rthread;

  if (argc > 1) { // On recupere l'argument si il existe
    port = atoi(argv[1]);
  }

  socketNum = socketServer(port,TCP);
  if (socketNum == -1) { // Cration du serveur
    printf("Erreur creation serveur : %s\n", (char*)perror);
    return EXIT_FAILURE;
  }else{
    printf("Serveru crée sur le socket : %d\n",socketNum);
  }

  msgSock = accept(socketNum, NULL, NULL);
  if (msgSock == -1) { // Creation du client
    printf("Erreur accept : %s\n", (char*)perror);
    return EXIT_FAILURE;
  }else{
    printf("Client connecté.\n");
  }

  /* Création du tube */
  if (pipe(pam) != 0 || pipe(map) != 0) {
     perror("pipe");
     return EXIT_FAILURE;
  }

  switch (fork()) {
    case -1:
      printf("ERREUR FORK\n");
      break;

    case 0:
      close(pam[1]);
      close(map[0]);
      dup2(pam[0], 0);
      dup2(map[1], 1);

      execlp("morpho","morpho", NULL, NULL);
      break;

    default:
      close(pam[0]);
      close(map[1]);

      //Création des threads
      pthread_create(&rthread, NULL, readerThread, (void*)&msgSock);
      pthread_create(&wthread, NULL, writerThread, (void*)&msgSock);
      
      while (pthread_tryjoin_np(rthread, NULL) != 0 && pthread_tryjoin_np(wthread, NULL) != 0) {
        usleep(500);
      }
      printf("Sortie\n");

      break;
    }

  close(msgSock);
  close(socketNum);
  return 0;
}
