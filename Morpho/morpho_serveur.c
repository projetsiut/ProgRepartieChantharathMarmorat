#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>          /* Pour pid_t (fork, getpid, getppid) */
#include <sys/socket.h>
#include "Reseau.h"

#define MAX 512

int main(int argc, char const *argv[]) {
  int pam[2], map[2];                 /* les deux descripteurs de fichier associés au tube */
  char* texte = (char*)calloc(MAX,sizeof(char));
  char result[MAX];
  //Port par défaut
  int port = 2553;

  //Permet à l'utilisateur d'entrer le port qu'il désire utiliser
  if (argc > 1) {
    port = atoi(argv[1]);
  }

  int socketNum,msgSock;
  //initialise le serveur sur le port donné
  socketNum = socketServer(port,TCP);
  if (socketNum == -1) {
    printf("Erreur creation serveur : %s\n", (char*)perror);
    return EXIT_FAILURE;
  }else{
    printf("Serveur crée sur le socket : %d\n",socketNum);
  }

  //Attend la connexion d'un client
  msgSock = accept(socketNum, NULL, NULL);
  if (msgSock == -1) {
    printf("Erreur accept : %s\n", (char*)perror);
    return EXIT_FAILURE;
  }else{
    printf("Client connecté.\n");
  }

  /* Création du tube */
  if (pipe(pam) != 0 || pipe(map) != 0) {
     perror("pipe");
     return EXIT_FAILURE;
  }

  switch (fork()) {
    case -1:
      printf("ERREUR FORK\n");
      break;

    //Le fils gère morpho de la même manière que l'exercice précédent
    case 0:
      close(pam[1]);
      close(map[0]);
      dup2(pam[0], 0);
      dup2(map[1], 1);

      execlp("morpho","morpho", NULL, NULL);
      break;

    //Le père lit le texte entré par le client sur le socket, l'envoie à morpho et écrit sur le socket le résultat de sorte à ce que ce dernier s'affiche sur la sortie du client
    default:
      close(pam[0]);
      close(map[1]);

      do {
          //Test si la lecture a fonctionné
          if (read(msgSock,texte,MAX) == -1) {
            perror("Read from socket : ");
            return EXIT_FAILURE;
          }
          //Si le texte entré par le client est vide, on ferme la connexion (break -> close)
          if (strlen(texte) == 0) { // On met == 1 pour \n
            write(msgSock,"Deconnexion\n",12);
            break;
          }
          //Test si l'écriture vers morpho a fonctionné
          if (write(pam[1],texte,strlen(texte)+1) == -1) {
            perror("Write to morpho : ");
            return EXIT_FAILURE;
          }
          //Test si la lecture depuis morpho a fonctionné
          if (read(map[0],result,MAX) == -1) {
            perror("Read from morpho : ");
            return EXIT_FAILURE;
          }
          //Test si l'écriture sur le socket a fonctionné
          if (write(msgSock,result,strlen(result)+1) == -1) {
            perror("Write to socket : ");
            return EXIT_FAILURE;
          }
          //nettoie les buffers
          memset(texte, 0, strlen(texte)+1);
          memset(result, 0, strlen(result)+1);
      } while(1);
      break;
    }

  free(texte);
  close(msgSock);
  close(socketNum);
  return 0;
}
