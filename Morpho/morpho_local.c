#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>          /* Pour pid_t (fork, getpid, getppid) */

#define MAX 512

int main(int argc, char const *argv[]) {

  pid_t ident;
  int pam[2], map[2];                 /* les deux descripteurs de fichier associés au tube */
  char* texte = (char*)calloc(MAX,sizeof(char));
  char result[MAX];

  /* Création du tube */
  if (pipe(pam) != 0 && pipe(map) != 0) {
     perror("pipe");
     return EXIT_FAILURE;
  }
  /*Création du processus fils
  Le fils redirige l'entree et sortie de morpho de tel sorte à ce qu'il lise et écrive dans le tube
  */
  ident = fork();

  if (ident == 0) {
    close(pam[1]);
    close(map[0]);

    dup2(pam[0], 0);
    dup2(1 ,map[1]);

    execlp("morpho","morpho", NULL);
  }else{
    close(pam[0]);
    close(map[1]);

    do {
        //On récupère le texte à analyser
        printf("\nEntrez un texte :");
        fgets(texte,MAX,stdin);
        //Si l'utilisateur entre un texte vide, on sort de la boucle
        if (strlen(texte) == 1) { // On met == 1 pour \n
          break;
        }
        //On écrit dans le tube le texte entré
        write(pam[1],texte,strlen(texte)+1);
        //On lit le résultat de morpho
        read(map[0],result,MAX);
        printf("Resultat lu : %s\n", result);
        fflush(stdout);
        usleep(10000); // temporise l'affichage

    } while(1);

  }

  free(texte);
  return 0;
}
