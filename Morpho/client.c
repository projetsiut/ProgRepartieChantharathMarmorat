/* ---------------------------------------------------------------------------
 * Programme client utilisant des sockets en mode STREAM (TCP).
 * Côté client : lecture de lignes au clavier et envoi au serveur jusqu'à lire
 *               une ligne vide, lecture et affichage des réponses du serveur.
 * Auteur     : Damien Genthial
 * Création   : Décembre 2002, adapté en 2005 pour affichage dans une fenêtre
 * 	       séparée. Méthode : lancer un xterm, y taper tty et relever le
 * 	       nom du pseudo-terminal, qu'on peut alors passer en paramètre
 * 	       4 ici.
 * Entrées    : Lit des lignes au clavier
 * Sorties    : Affiche la réponse du serveur sur l'écran
 * Paramètres : voir usage.
 */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdbool.h>
#include <pthread.h>
#include "Reseau.h"

void usage(void);
void sortie(const char message[]);
void* recopie(void*);

/* Taille des tampons de lecture et écriture */
#define MAX 128

/* Structure à passer en paramètre des threads de recopie */
typedef struct {
    int in;
    int out;
} Parametre;

int main(int nbArg, char *arg[])
{
    char buf[MAX+1]; /* Tampon pour communication et lecture clavier,
                      * le caractère supplémentaire sert à garantir la présence du NUL final. */

    if ((nbArg < 3) || (nbArg > 4))
        usage();

    /* Création du socket */
    int sock;
    if ((sock = socketClient(arg[1], atoi(arg[2]), TCP)) == -1)
        sortie("Erreur sur la connexion réseau");
    afficheInfosSocket(sock);

    /* Si un pseudo-terminal est donné, on l'utilise pour les sorties,
     * par défaut, c'est stdout (1) */
    int out = 1;
    if (nbArg == 4) {
        out = open(arg[3], O_APPEND | O_WRONLY, -1);
        if (out == -1) {
            fprintf(stderr, "Impossible d'écrire sur %s !\n", arg[3]);
            usage();
        }
    }

    /* On crée un thread chargé de recopier ce qui sort du réseau vers la sortie standard ou le pseudo-terminal.
     * On évite ainsi d'aller se bloquer sur le read suivant alors qu'on n'a pas fini de lire la réponse du serveur. */
    pthread_t thr;
    Parametre pAff = { sock, out };

    pthread_create(&thr, NULL, recopie, &pAff);

    buf[MAX] = '\0'; /* Jamais écrasé, donc toujours présent */
    while (true) { /* itérer */
        int longueur;

        fflush(stdout);
        printf("Client> : ");
        fgets(buf, MAX, stdin);

        if (buf[0] == '\n') break;

        longueur = strlen(buf);
        if (write(sock, buf, longueur + 1) == -1)
            sortie("Erreur en écriture sur le réseau");

        /* L'affichage est pris en charge par le thread */
    }

    /* On ferme le socket pour arrêter le thread d'affichage */
    shutdown(sock, 1); /* 0 = receive, 1 = send, 2 = both */

    /* On attend que l'affichage soit bien fini pour arrêter complètement le programme */
    pthread_join(thr, NULL);

    return EXIT_SUCCESS;
}

/* Met fin au programme en cas d'erreur */
void sortie(const char message[])
{
    perror(message);
    exit(EXIT_FAILURE);
}

void usage(void)
{
    fprintf(stderr, "Usage : \n\tclient serveur port [ pseudo_term ]\n");
    exit(EXIT_FAILURE);
}

/* Thread de recopie du flot param->in vers le flot param->out */
void * recopie(void *param)
{
    char bufOut[MAX];
    int nbLus;
    Parametre flots = *((Parametre*) param);

    while (true) { /* itérer jusqu'à fin du flot d'entrée */
        nbLus = read(flots.in, bufOut, MAX);

        if (nbLus == -1)
            sortie("recopie : erreur en lecture sur le flot.");
        else if (nbLus == 0) { /* flot fermé */
            printf("recopie : fermeture du flot.\n");
            break;
        }

        write(flots.out, bufOut, nbLus);
    }
    close(flots.out);
    return NULL;
}
