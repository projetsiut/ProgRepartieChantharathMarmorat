package nim;

import java.rmi.*;
import java.rmi.registry.*;

/** Affichage des noms enregistrés dans le registry (pour débogage)
 */
public class NomsEnregistres {
    
    // Affiche un mode d'emploi succinct
    private static void usage() {
        System.err.println("Usage : ");
        System.err.println("\tjava nim.NomEnregistres [serveur[:port]]");
        System.exit(1);
    }

    public static void main(String[] args) throws Exception {
        // Positionner le chemin de recherche des classes RMI sur le répertoire courant
        String cwd = System.getProperty("user.dir");
        System.out.println("PWD = " + cwd);
        System.setProperty ("java.rmi.server.codebase", "file://"+cwd+"/");
        System.out.println("codebase = " + System.getProperty ("java.rmi.server.codebase"));

        // Analyse des arguments
        String host = "localhost";
        int port = 1099;
        switch (args.length) {
            case 0:
                break;
            case 1:
                String[] remote = args[0].split(":"); 
                host = remote[0];
                if (remote.length > 1)
                    port = new Integer(remote[1]).intValue();
                else 
                    port = 1099;
                break;
            default:
                usage();
        }

        // Affichage des noms enregistrés dans le registry (pour débogage)
        Registry reg = null;
        try {
            reg = LocateRegistry.getRegistry(host, port);
            String[] liste = reg.list();
            System.out.println("Noms enregistrés : ");
            for (int i = 0; i < liste.length; i++) System.out.println(liste[i]);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    } 
}
