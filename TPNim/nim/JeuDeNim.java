package nim;
import java.rmi.*;

/** 
 * Interface pour le jeu de Nim réparti.
 * @author  Damien Genthial - Université de Grenoble Alpes - IUT de Valence 
 * @version 11 mars 2008
 */
public interface JeuDeNim extends java.rmi.Remote {
   /** Démarre une nouvelle partie
    *  @throws java.rmi.RemoteException si l'objet distant est injoignable
    *  @return identifiant de la partie (entier dans 1..MAX) ou -1 si on a atteint le 
    *          nombre maximum de parties */
   public int nouvellePartie() throws RemoteException;

   /** Joindre une partie
    *  @throws java.rmi.RemoteException si l'objet distant est injoignable
    *  @param id identifiant de la partie à rejoindre
    *  @param nom du joueurs à inscrire
    *  @return numéro de joueur (1 ou 2) ou -1 si la partie n'existe pas
    *          ou est déjà complète */
   public int joindre(int id, String nom) throws RemoteException;

   /** Terminer une partie pour un joueur (sans effet si la partie est inconnue)
    *  @throws java.rmi.RemoteException si l'objet distant est injoignable
    *  @param id identifiant de la partie à terminer
    *  @param numJoueur numéro du joueur qui a terminé
    *  Appelée par le client quand il n'y a plus d'allumettes à retirer */
   public void terminer(int id, int numJoueur) throws RemoteException;

   /** La partie est-elle complète ? Permet d'attendre qu'il y ait deux joueurs pour démarrer
    *  @throws java.rmi.RemoteException si l'objet distant est injoignable
    *  @param id identifiant de la partie
    *  @return vrai si la partie est complète (2 joueurs). */
   public boolean estComplete(int id) throws RemoteException;

   /** Détermine qui doit jouer le prochain coup
    *  @throws java.rmi.RemoteException si l'objet distant est injoignable
    *  @param id identifiant de la partie
    *  @return numéro du joueur qui doit jouer le prochain coup ou -1 si la partie est inconnue */
   public int tour(int id) throws RemoteException; 

   /** Nombre d'allumettes restantes
    *  @throws java.rmi.RemoteException si l'objet distant est injoignable
    *  @param id identifiant de la partie
    *  @return le nombre d'allumettes restantes ou -1 si la partie est inconnue */
   public int restantes(int id) throws RemoteException; 

   /** Retirer n allumettes dans une partie (sans effet si la partie est inconnue)
    *  @throws java.rmi.RemoteException si l'objet distant est injoignable
    *  @param id identifiant de la partie
    *  @param n nombre d'allumettes à ôter */
   public void retirer(int id, int n) throws RemoteException;

   /** Construction d'une chaîne représentant la liste des parties
    *  @throws java.rmi.RemoteException si l'objet distant est injoignable
    * @return chaîne représentant la liste des parties */
   public String partiesEnCours() throws RemoteException;
}
