package nim;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Random;

public class JeuDeNimImpl extends UnicastRemoteObject implements nim.JeuDeNim {

    private HashMap<Integer, Partie> parties;
    private Random rGen;

    public JeuDeNimImpl() throws RemoteException {
        super();
        this.parties = new HashMap<>();
        this.rGen = new Random();
    }

    @Override
    public int nouvellePartie() throws RemoteException {
        int gen = this.rGen.nextInt() & Integer.MAX_VALUE;
        this.parties.put(gen, new Partie(15, 20));
        return gen;
    }

    @Override
    public int joindre(int id, String nom) throws RemoteException {
        return this.parties.get(id).inscrire(nom);
    }

    @Override
    public void terminer(int id, int numJoueur) throws RemoteException {
        this.parties.get(id).desinscrire(numJoueur);
        if(this.parties.get(id).nbJoueurs() == 0)
            this.parties.remove(id);
    }

    @Override
    public boolean estComplete(int id) throws RemoteException {
        return this.parties.get(id).nbJoueurs() == 2;
    }

    @Override
    public int tour(int id) throws RemoteException {
        return this.parties.get(id).prochain();
    }

    @Override
    public int restantes(int id) throws RemoteException {
        return this.parties.get(id).nb();
    }

    @Override
    public void retirer(int id, int n) throws RemoteException {
        this.parties.get(id).jouer(n);
    }

    @Override
    public String partiesEnCours() throws RemoteException {
        String currentRunningGame = "";

        for (int cur : this.parties.keySet()) {
            currentRunningGame+="Pid : "+cur+" | "+this.parties.get(cur).toString()+"\n";
        }

        return currentRunningGame;
    }
}
