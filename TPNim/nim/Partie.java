package nim;

import java.util.Random;
import java.util.Hashtable;

/** 
 * Classe pour la réalisation d'un jeu de Nim réparti.
 * @author  Damien Genthial - Université Grenoble Alpes - IUT de Valence 
 * @version 20130301
 */
public class Partie {
    /** Nombre d'allumettes restantes */
    private int xNb; 

    /** Prochain joueur (1 ou 2) */
    private int xProchain;

    /** Liste des joueurs */
    private Hashtable<Integer, String> xJoueurs;

    /** Initialisation de la liste des joueurs */
    private void initJoueurs() {
        this.xJoueurs = new Hashtable<Integer, String>();
    }

    /** Nouvelle partie
     *  @param n nombre initial d'allumettes */
    public Partie(int n) {
        this.xNb = n;
        this.initJoueurs();
        this.xProchain = 1; 
    }

    /** Nouvelle partie avec un nombre initial d'allumettes tiré aléatoirement
     *  entre min et max (inclus).
     *  @param min nombre minimal d'allumettes
     *  @param max nombre maximal d'allumettes */
    public Partie(int min, int max) {
        Random r = new Random(System.currentTimeMillis());
        this.xNb = r.nextInt(max - min + 1) + min;
        this.initJoueurs();
        this.xProchain = 1; 
    }

    /** Nombre d'allumettes restantes 
     *  @return le nombre d'allumettes restantes dans la partie */
    public int nb() { return this.xNb; }

    /** Nombre de joueurs inscrits
     *  @return le nombre de joueurs inscrits à cette partie */
    public int nbJoueurs() { return this.xJoueurs.size(); }

    /** Numéro du prochain joueur
     *  @return le numéro du joueur qui doit jouer le prochain coup */
    public int prochain() { return this.xProchain; }

    /** Inscription d'un joueur à cette partie
     *  @param nom du joueur
     *  @return numéro du joueur (1 ou 2), ou -1 s'il y a déjà deux joueurs. */
    public synchronized int inscrire(String nom) {
        int nbJoueurs = this.xJoueurs.size();
        if (nbJoueurs == 2)
            return -1;
        this.xJoueurs.put(nbJoueurs + 1, nom);
        //System.out.println(nom + " rejoint la partie\n" + this + "\n");
        return nbJoueurs + 1;
    }

    /** Désinscription d'un joueur à cette partie, appelée par chaque joueur quand il sait que la partie est finie,
     *  sans effet si le joueur est inconnu
     *  @param numJoueur numéro du joueur (1 ou 2) */
    public synchronized void desinscrire(int numJoueur) {
        if ((numJoueur != 1) && (numJoueur != 2)) return;
        this.xJoueurs.remove(numJoueur);
    }

    /** Pour affichage de l'état de la partie sous forme d'une simple chaîne */
    public String toString() {
        int nbJoueurs = this.xJoueurs.size();
        if (nbJoueurs == 0)
            return "Pas de joueur inscrit !";
        else if (nbJoueurs == 1)
            return "Un seul joueur inscrit : " + this.xJoueurs + " !";

        String res = this.xNb + " allumettes : ";
        for (int i = 0; i < this.xNb; i++) res += "| ";
        res += "- " + this.xJoueurs.get(1) + "/" + this.xJoueurs.get(2) + ", " + this.xJoueurs.get(this.xProchain) + " va jouer.";
        return res;
    }

    /** jouer un coup (ne fait rien s'il n'y a pas deux joueurs inscrits)
     *  @param n nombre d'allumettes à retirer */
    public synchronized void jouer(int n) {
        if (this.xJoueurs.size() < 2)
            return;
        // Sinon on enregistre le coup
        this.xNb = this.xNb - n;
        if (this.xNb < 0) this.xNb = 0;
        this.xProchain = 3 - this.xProchain; // 3-2=1 et 3-1=2
    }

    // Pour tester
    public static void main(String[] args) {
        try {
            Partie p = new Partie(22);
            System.out.println(p);
            p.inscrire("Martin");
            System.out.println(p);
            p.inscrire("Georges");
            System.out.println(p);
            System.out.println("Le joueur retire 4 allumettes.");
            p.jouer(4);
            System.out.println(p);
            System.out.println("Le joueur retire 2 allumettes.");
            p.jouer(2);
            System.out.println(p);
            System.out.println("Désinscription du joueur 3 => ");
            p.desinscrire(3);
            System.out.println(p);
            System.out.println("Désinscription du joueur 1 => ");
            p.desinscrire(1);
            System.out.println(p);
            System.out.println("Désinscription du joueur 2 => ");
            p.desinscrire(2);
            System.out.println(p);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
